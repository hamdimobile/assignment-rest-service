package com.example.restdemo.exceptions;

import org.springframework.http.HttpStatus;

@SuppressWarnings("serial")
public class GeneralException extends Exception {

  private static final long serialVersionUID = 12345678987654321L;
  private final HttpStatus httpStatus;

  public GeneralException(String message, HttpStatus httpStatus) {
    super(message);
    this.httpStatus = httpStatus;
  }

  public GeneralException(String message) {
    super(message);
    this.httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
  }

  public HttpStatus getHttpStatus() {
    return httpStatus;
  }

}
