package com.example.restdemo.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import jakarta.validation.Valid;
import jakarta.validation.constraints.NotBlank;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
public class SampleRequestDto {

  @Valid
  @JsonProperty("sampleservicerq")
  private Sampleservicerq sampleservicerq;

  @Data
  @NoArgsConstructor
  @AllArgsConstructor
  public static class Sampleservicerq {

    @JsonProperty("service_id")
    @NotBlank(message = "service_id cannot be blank")
    private String serviceId;

    @JsonProperty("order_type")
    @NotBlank(message = "order_type cannot be blank")
    private String orderType;

    @JsonProperty("type")
    private String type;

    @JsonProperty("trx_id")
    private String trxId;

  }

}
