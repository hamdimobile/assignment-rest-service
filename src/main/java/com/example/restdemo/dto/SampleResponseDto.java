package com.example.restdemo.dto;

import com.example.restdemo.util.Constants;
import com.fasterxml.jackson.annotation.JsonProperty;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
public class SampleResponseDto {

  @JsonProperty("sampleservicers")
  @NotNull(message = "sampleservicers cannot be null")
  private Sampleservicers sampleservicers;

  @Data
  @NoArgsConstructor
  public static class Sampleservicers {

    @JsonProperty("error_code")
    @NotBlank(message = "error_code cannot be blank")
    private String errorCode;

    @JsonProperty("error_msg")
    @NotBlank(message = "error_msg cannot be blank")
    private String errorMsg;

    @JsonProperty("trx_id")
    private String trxId;

    public Sampleservicers(String trxId) {
      super();
      this.errorCode = Constants.ERROR_CODE;
      this.errorMsg = Constants.ERROR_MESSAGE;
      this.trxId = trxId;
    }

  }

}
