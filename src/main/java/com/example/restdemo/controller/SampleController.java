package com.example.restdemo.controller;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.restdemo.dto.SampleRequestDto;
import com.example.restdemo.dto.SampleResponseDto;
import com.example.restdemo.exceptions.GeneralException;
import com.example.restdemo.service.SampleService;

import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;

@RestController
@RequestMapping("/external/services/rest")
@RequiredArgsConstructor
public class SampleController {

  private final SampleService service;

  @PostMapping("/sample-service")
  public SampleResponseDto sampleServicbe(@Valid @RequestBody SampleRequestDto request) throws GeneralException {
    return service.execute(request);
  }
}
