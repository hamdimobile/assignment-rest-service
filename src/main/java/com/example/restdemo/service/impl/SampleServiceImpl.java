package com.example.restdemo.service.impl;

import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.example.restdemo.dto.SampleRequestDto;
import com.example.restdemo.dto.SampleResponseDto;
import com.example.restdemo.dto.SampleResponseDto.Sampleservicers;
import com.example.restdemo.exceptions.GeneralException;
import com.example.restdemo.service.SampleService;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class SampleServiceImpl implements SampleService {

  @Override
  public SampleResponseDto execute(SampleRequestDto request) throws GeneralException {
    // business validation
    validate(request);

    // construct response
    return contructResponse(request);
  }

  private void validate(SampleRequestDto request) throws GeneralException {
    if (!StringUtils.hasText(request.getSampleservicerq().getTrxId())) {
      log.warn("we know trx_id is optional as per requirement, but at least you can put any random value to make it better");
    }
  }

  private SampleResponseDto contructResponse(SampleRequestDto request) {
    var sampleservicers = new Sampleservicers(request.getSampleservicerq().getTrxId());
    return SampleResponseDto.builder().sampleservicers(sampleservicers).build();
  }
}
