package com.example.restdemo.service;

import com.example.restdemo.dto.SampleRequestDto;
import com.example.restdemo.dto.SampleResponseDto;
import com.example.restdemo.exceptions.GeneralException;

public interface SampleService {

  public SampleResponseDto execute(SampleRequestDto request) throws GeneralException;

}
