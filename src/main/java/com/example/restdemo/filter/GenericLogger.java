package com.example.restdemo.filter;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;

import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;
import org.springframework.web.util.ContentCachingRequestWrapper;
import org.springframework.web.util.ContentCachingResponseWrapper;

import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
public class GenericLogger extends OncePerRequestFilter {

  /**
   * Spring framework hidden gems Log each request and response with full Request
   * URI, content payload and duration of the request in ms.
   *
   * @param request     the request
   * @param response    the response
   * @param filterChain chain of filters
   * @throws ServletException
   * @throws IOException
   */
  @Override
  protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
      throws ServletException, IOException {

    Date timestamp = new Date();

    // apply some stronger magic here :-)
    ContentCachingRequestWrapper wrappedRequest = new ContentCachingRequestWrapper(request);
    ContentCachingResponseWrapper wrappedResponse = new ContentCachingResponseWrapper(response);

    filterChain.doFilter(wrappedRequest, wrappedResponse);

    String requestBody = this.getContentAsString(wrappedRequest.getContentAsByteArray(),
        request.getCharacterEncoding());
    String responseBody = this.getContentAsString(wrappedResponse.getContentAsByteArray(),
        response.getCharacterEncoding());

    StringBuilder sb = new StringBuilder();
    try {
      sb.append("\r\n========== START =============== \r\n");
      sb.append("Timestamp\t:\t").append(new SimpleDateFormat("dd/MM/yyyy HH:mm:ss.SSS").format(timestamp))
          .append("\r\n");
      sb.append("Accessing\t:\t").append(request.getRequestURL()).append("\r\n");
      sb.append("Method\t\t:\t").append(request.getMethod()).append("\r\n");

      HashMap<String, String> mapHeaderInfo = new HashMap<>();
      Enumeration<String> headerNames = request.getHeaderNames();
      while (headerNames.hasMoreElements()) {
        String key = headerNames.nextElement();
        String value = request.getHeader(key);
        mapHeaderInfo.put(key, value);
      }
      sb.append("Headers\t\t:\t").append(mapHeaderInfo).append("\r\n");
      sb.append("Query\t\t:\t").append(request.getQueryString() == null ? "" : request.getQueryString()).append("\r\n");
      sb.append("Request\t\t:\t").append(requestBody).append("\r\n");
      sb.append("Response\t:\t").append(responseBody).append("\r\n");

    } catch (Exception e) {
      sb.append("Error \t\t:\t").append(e.getCause()).append("\r\n");
      for (StackTraceElement elem : e.getStackTrace()) {
        sb.append("Error \t\t:\t\t").append(elem.toString()).append("\r\n");
      }
    } finally {
      sb.append("Code\t\t:\t").append(response.getStatus()).append("\r\n");
      sb.append("Time\t\t:\t").append(System.currentTimeMillis() - timestamp.getTime()).append("ms \r\n");
      sb.append("==========  END  =============== \r\n");

      log.info("{}", sb);
    }

    wrappedResponse.copyBodyToResponse(); // IMPORTANT: copy content of response back into original response
  }

  private String getContentAsString(byte[] buf, String charsetName) {
    if (buf == null || buf.length == 0)
      return "";
    try {
      return new String(buf, 0, buf.length, charsetName);
    } catch (UnsupportedEncodingException ex) {
      return "Unsupported Encoding";
    }
  }
}
