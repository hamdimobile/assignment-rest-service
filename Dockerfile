FROM eclipse-temurin:17

WORKDIR /opt/app
COPY ./target/rest-service.jar rest-service.jar
COPY ./src/main/resources/application.yml application.yml

EXPOSE 8080
ENV RESTSERVICE_SERVER_PORT=8080

RUN date
ENV TZ=Asia/Jakarta
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone
RUN date

ENTRYPOINT exec java -jar rest-service.jar
